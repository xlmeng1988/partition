#include "Node_PPort_Edge_cluster.h"


/**
 * judge whether the block is sequential or not
 * @param block to be judge
 * @return a bool value
 */
bool isSequentialBlock( Block *block )
{
    int defId = block->getBlockDefId();
    return (defId >= BLOCK_CS_REG_PRIM && defId <= BLOCK_CS_REG_RS_PRIM);
}

/**
 * judge whether the block is combinational or not
 * @param block to be judge
 * @return a bool value
 */
bool isCombinationalBlock( Block *block )
{
    if ( block->getBlockDef()->linkedNetwork() != NULL ) {
        return true;
    }

    return !isSequentialBlock(block);
}

