#include"Cluster_func.h"
//#include"Merge_func.h"
static string legalizeName(string name)
{
    string legalName="";
    for (unsigned int i=0; i<name.size(); i++)
    {
        char c = name[i];
        if (c=='/' || c=='(' || c==')' || c=='[' || c== ']' ||c=='\\')
            legalName = legalName + "_";
        else
            legalName = legalName + c;
        }
    return legalName;
}

void dataPreparation(vector<Block*>&all_block,vector<Port*>&all_port,vector<Net*>&all_net,Network* &network,int&seq_logic_num,vector<Port*>&bportWithoutNet){
    all_block.reserve(network->getNumBlocks()+10);
    all_port.reserve(network->getNumPorts()+10);
    all_net.reserve(network->getNumNets()+10);


 //   int seq_logic_num(0);
    GeneralIterator<Block*> blocks = network->getAllBlocks();
    while(blocks.hasNext())
    {
        Block* block = blocks.next();
        if(isCombinationalBlock(block)==false)
        {
            seq_logic_num++;
        }
        all_block.push_back(block);
    }
    //cout<<"seq logic num: "<<seq_logic_num<<endl;

    int all_seq_net(0);
    GeneralIterator<Net*> nets = network->getAllNets();
    while(nets.hasNext())
    {
        Net* net = nets.next();
        if((net->getFaninNumber()!=0)&&(net->getFanoutNumber()!=0))
        {
            all_net.push_back(net);
        }
        else if((net->getFaninNumber()==0)&&(net->getFanoutNumber()!=0))
        {
            all_net.push_back(net);
        }
		else
		{
			network->deleteNet(net);
		}
    }

	 GeneralIterator<Port*> ports = network->getAllPorts();
    while(ports.hasNext())
    {
        Port* port = ports.next();
        if(port->getNet() == NULL)
        {
			bportWithoutNet.push_back(port);
         //   cout<<"boundry port: "<<port->getName()<<"has no net"<<endl;
        }
        else{
        all_port.push_back(port);
        }
    }






    for(unsigned int count_x=0;count_x<all_net.size();count_x++)
    {
        int dst_num = all_net[count_x]->getFanoutNumber();
        int src_num = all_net[count_x]->getFaninNumber();
        if((src_num==0)&&(dst_num==0))
        {
            cout<<"count x: "<<count_x<<"  no dst no src"<<endl;
        }
        if((src_num==0)&&(dst_num!=0))
        {
            cout<<"count x: "<<count_x<<"  have dst no src"<<endl;
        }
        if((src_num!=0)&&(dst_num==0))
        {
            cout<<"count x: "<<count_x<<"  no dst have src"<<endl;
        }
    }
    cout<<"Block Num: "<<all_block.size()<<endl;
    cout<<"Boundry Port: "<<network->getNumPorts()<<endl;
    cout<<"Net Num: "<<all_net.size()<<endl;
}
void clusterDataConstructor(vector<Node>&all_node,vector<Node*>&Seq_node,vector<Node*>&Comb_node,vector<Edge>&all_edge,vector<vector<PPort> >&all_PPort,vector<Block*>&all_block,vector<Net*>&all_net,vector<Port*>all_port){
    for(unsigned int count_x=0;count_x<all_node.size();count_x++)
{
    all_node[count_x].block = all_block[count_x];
    all_block[count_x]->setUserData(&all_node[count_x]);

    all_node[count_x].is_comb = isCombinationalBlock(all_node[count_x].block);//before modifed, here using all_node[count_x].block->isCombinationl()
    all_node[count_x].Node_name = all_node[count_x].block->getName();
    if(all_node[count_x].is_comb == false)
    {
        Seq_node.push_back(&all_node[count_x]);
    }
    else
    {
        Comb_node.push_back(&all_node[count_x]);
    }
}

//-------------------create Edge-----------------------------------------
for(unsigned int count_x=0;count_x<all_edge.size();count_x++)
{
    all_edge[count_x].net = all_net[count_x];
    all_net[count_x]->setUserData(&all_edge[count_x]);
    all_edge[count_x].Edge_name = all_edge[count_x].net->getName();
}
//--------------------create PPort on Node (port on block)---------------------

for(unsigned int count_x = 0;count_x<all_PPort.size()-1;count_x++)
{
    GeneralIterator<Port*> port_on_block = all_block[count_x]->getAllPorts();
    all_PPort[count_x].reserve(all_block[count_x]->getInoutNumber()+all_block[count_x]->getInputNumber()+all_block[count_x]->getOutputNumber()+5);
    while(port_on_block.hasNext())
    {
        Port* temp_port = port_on_block.next();
        PPort temp_pport = PPort(temp_port);
        temp_pport.PPort_name = temp_port->getName();
        all_PPort[count_x].push_back(temp_port);
        all_PPort[count_x][all_PPort[count_x].size()-1].PPort_name = temp_port->getName();
    }
}

//-----------------------create PPort on Boundry ---------------------------
for(unsigned int count_x = 0;count_x<all_port.size();count_x++)
{
    PPort temp_pport = PPort(all_port[count_x]);
    temp_pport.isBoundryPPort = true;
    temp_pport.PPort_name = temp_pport.port->getName();
    all_PPort[all_PPort.size()-1].push_back(temp_pport);
    all_PPort[all_PPort.size()-1][all_PPort[all_PPort.size()-1].size()-1].PPort_name = temp_pport.PPort_name;
}
for(unsigned int count_x = 0;count_x<all_PPort.size();count_x++)
{
    for(unsigned int count_y = 0;count_y<all_PPort[count_x].size();count_y++)
    {
        all_PPort[count_x][count_y].port->setUserData(&all_PPort[count_x][count_y]);
    }
}

//--------link Node-PPort & PPort-Node------------------------------------------------------------------
for(unsigned int count_x=0;count_x<all_node.size();count_x++)
{
    GeneralIterator<Port*> input_port = all_block[count_x]->getInputPorts();
    while(input_port.hasNext())
    {
        Port* temp_p = input_port.next();
        PPort* temp_pp = (PPort*)temp_p->getUserData();
        all_node[count_x].in_pport.push_back(temp_pp);
        all_node[count_x].all_pport.push_back(temp_pp);
    }
    GeneralIterator<Port*> output_port = all_block[count_x]->getOutputPorts();
    while(output_port.hasNext())
    {
        Port* temp_p = output_port.next();
        PPort* temp_pp = (PPort*)temp_p->getUserData();
        all_node[count_x].out_pport.push_back(temp_pp);
        all_node[count_x].all_pport.push_back(temp_pp);
    }
    GeneralIterator<Port*> inout_port = all_block[count_x]->getInoutPorts();
    while(inout_port.hasNext())
    {
        Port* temp_p = inout_port.next();
        PPort* temp_pp = (PPort*)temp_p->getUserData();
        all_node[count_x].inout_pport.push_back(temp_pp);
        all_node[count_x].all_pport.push_back(temp_pp);
    }
}
//--------link PPort-Node  &  PPort - Edge-------------------
for(unsigned int count_x=0;count_x<all_PPort.size()-1;count_x++)//first handle port-on-block
{
    for(unsigned int count_y=0;count_y<all_PPort[count_x].size();count_y++)
    {
        Block* temp_block = all_PPort[count_x][count_y].port->getBlock();
        if(temp_block == NULL)
        {
            cout<<"wrong!"<<endl;
        }
        all_PPort[count_x][count_y].srcNode = (Node*)temp_block->getUserData();


        Net* temp_net = all_PPort[count_x][count_y].port->getNet();
        if(temp_net != NULL)
        {
            all_PPort[count_x][count_y].edge = (Edge*)temp_net->getUserData();
        }
    }
}

for(unsigned int count_x=0;count_x<all_port.size();count_x++)// handle the port on boundry
{
    Net* temp_net = all_PPort[all_PPort.size()-1][count_x].port->getNet();
    if(temp_net != NULL)
    {
        all_PPort[all_PPort.size()-1][count_x].edge = (Edge*)temp_net->getUserData();
    }
}

//---------------link Edge-PPort---------------------------------------------------
for(unsigned int count_x=0;count_x<all_edge.size();count_x++)
{
    //handle the edge's source pport
    if(all_edge[count_x].net->getSrcPort() != NULL)
    {
    Port* temp_src_p = all_edge[count_x].net->getSrcPort();
    all_edge[count_x].src_pport = (PPort*)temp_src_p->getUserData();
    }
    GeneralIterator<Port*> temp_dst_p = all_edge[count_x].net->getDstPorts();
    while(temp_dst_p.hasNext())
    {
        Port* temp_dst = temp_dst_p.next();
        PPort* temp_pp = (PPort*)temp_dst->getUserData();
        all_edge[count_x].dst_pport.push_back(temp_pp);
    }

}


}
void preprocessing(vector<Edge*>&final_edge,vector<PPort*>&all_boundry_pport,vector<PPort>&new_boundry_pport,vector<Edge>&new_edge,vector<Cutpair>&cutPairs,vector<Node*>&Seq_node,vector<vector<PPort> >&all_PPort,vector<Edge>&all_edge,Network* &network){
        int edge_need_cut(0);
    //Count how many new boundry should be added

for(unsigned int count_x=0;count_x<Seq_node.size();count_x++)
{
    for(unsigned int count_y=0;count_y<Seq_node[count_x]->all_pport.size();count_y++)
    {
        if(Seq_node[count_x]->all_pport[count_y]->edge != NULL)
        {
            edge_need_cut = edge_need_cut+1;
        }
    }
}

int total_new_bp = edge_need_cut*2 + 20;



//this reserve can make sure that push_back operation would not change the address of other parameters
new_boundry_pport.reserve(total_new_bp);
new_edge.reserve(edge_need_cut+20);
cutPairs.reserve(edge_need_cut+10);

int new_edge_id(0);

for(unsigned int count_x=0;count_x<Seq_node.size();count_x++)
{
        for(unsigned int count_y=0;count_y<Seq_node[count_x]->in_pport.size();count_y++)
        {
            if(Seq_node[count_x]->in_pport[count_y]->edge!=NULL)
            {
                Cutpair temp_cutpair;
                PPort temp_Ipport;
                PPort temp_Opport;
                temp_Ipport.srcNode =NULL;
                temp_Opport.srcNode =NULL;

                temp_Ipport.isBoundryPPort = true;
                temp_Opport.isBoundryPPort = true;

                char id[30];
                string ID;
                sprintf(id,"%d",Seq_node[count_x]->in_pport[count_y]->edge->Num_for_cut);
            //	itoa(Seq_node[count_x]->in_pport[count_y]->edge->Num_for_cut,id,10);
                ID = id;
                temp_Ipport.PPort_name = Seq_node[count_x]->in_pport[count_y]->edge->Edge_name + ":BP"+ID;
                temp_Ipport.PPort_name = legalizeName(temp_Ipport.PPort_name);
                Seq_node[count_x]->in_pport[count_y]->edge->Num_for_cut++;
                temp_cutpair.PI_name = temp_Ipport.PPort_name;
                sprintf(id,"%d",Seq_node[count_x]->in_pport[count_y]->edge->Num_for_cut);
            //	itoa(Seq_node[count_x]->in_pport[count_y]->edge->Num_for_cut,id,10);
                ID = id;
                temp_Opport.PPort_name = Seq_node[count_x]->in_pport[count_y]->edge->Edge_name + ":BP"+ID;
                temp_Opport.PPort_name = legalizeName(temp_Opport.PPort_name);
                Seq_node[count_x]->in_pport[count_y]->edge->Num_for_cut++;
                temp_cutpair.PO_name = temp_Opport.PPort_name;


                //------add new port defination into network, their connect-information is empty(which is not essential)

                network->getDefManager()->addPortDef(temp_Ipport.PPort_name,DIR_IN);
                temp_Ipport.port = network->addPort(temp_Ipport.PPort_name);

                network->getDefManager()->addPortDef(temp_Opport.PPort_name,DIR_OUT);
                temp_Opport.port = network->addPort(temp_Opport.PPort_name);

                //updata edge & New PO connect-information
                new_boundry_pport.push_back(temp_Opport);
                temp_cutpair.PO_pport = &new_boundry_pport[new_boundry_pport.size()-1];

                for(unsigned count_z=0;count_z<Seq_node[count_x]->in_pport[count_y]->edge->dst_pport.size();count_z++)
                {
                    if(Seq_node[count_x]->in_pport[count_y]->edge->dst_pport[count_z] == Seq_node[count_x]->in_pport[count_y])
                    {
                        new_boundry_pport[new_boundry_pport.size()-1].edge = Seq_node[count_x]->in_pport[count_y]->edge;// this sentence should put in front of the next one
                        Seq_node[count_x]->in_pport[count_y]->edge->dst_pport[count_z] = &new_boundry_pport[new_boundry_pport.size()-1];
                        break;
                    }
                    if(count_z==Seq_node[count_x]->in_pport[count_y]->edge->dst_pport.size())
                    {
                        cout<<"wrong in updata edge's connect-information"<<endl;
                    }
                }
                //updata the original Port & new egde & New PI connect-information
                Edge temp_new_edge;
                char edge_id[20];
                string edge_ID;
                sprintf(edge_id,"%d",new_edge_id);
                edge_ID = edge_id;
                temp_new_edge.Edge_name = "new_edge_"+edge_ID;
                temp_new_edge.Edge_name = legalizeName(temp_new_edge.Edge_name);
                new_edge_id++;
                temp_new_edge.net = network->addNet(temp_new_edge.Edge_name);// add new edge to the network, but it's connection-information is empty(which is not eseential)
                new_edge.push_back(temp_new_edge);
                new_boundry_pport.push_back(temp_Ipport);

                temp_cutpair.PI_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                new_boundry_pport[new_boundry_pport.size()-1].edge = &new_edge[new_edge.size()-1];
                new_edge[new_edge.size()-1].src_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                Seq_node[count_x]->in_pport[count_y]->edge = &new_edge[new_edge.size()-1];
                new_edge[new_edge.size()-1].dst_pport.push_back(Seq_node[count_x]->in_pport[count_y]);
                new_edge[new_edge.size()-1].Edge_name = new_edge[new_edge.size()-1].dst_pport[0]->PPort_name +"_to_"+new_edge[new_edge.size()-1].src_pport->PPort_name;
                new_edge[new_edge.size()-1].Edge_name = legalizeName(new_edge[new_edge.size()-1].Edge_name);

                temp_cutpair.new_edge_name = new_edge[new_edge.size()-1].Edge_name;
                temp_cutpair.New_edge_connect_PI_pport = true;
                cutPairs.push_back(temp_cutpair);
            }
        }

        for(unsigned int count_y=0;count_y<Seq_node[count_x]->out_pport.size();count_y++)
        {
            if(Seq_node[count_x]->out_pport[count_y]->edge !=NULL)
            {
                Cutpair temp_cutpair;
                PPort temp_Ipport;
                PPort temp_Opport;
                temp_Ipport.srcNode =NULL;
                temp_Opport.srcNode =NULL;

                temp_Ipport.isBoundryPPort = true;
                temp_Opport.isBoundryPPort = true;
                char id[30];
                string ID;

                sprintf(id,"d%",Seq_node[count_x]->out_pport[count_y]->edge->Num_for_cut);
                ID = id;
                temp_Ipport.PPort_name = Seq_node[count_x]->out_pport[count_y]->edge->Edge_name + ":BP"+ID;
                temp_Ipport.PPort_name = legalizeName(temp_Ipport.PPort_name);
                Seq_node[count_x]->out_pport[count_y]->edge->Num_for_cut ++;
                temp_cutpair.PI_name = temp_Ipport.PPort_name;

                sprintf(id,"%d",Seq_node[count_x]->out_pport[count_y]->edge->Num_for_cut);
                ID = id;
                temp_Opport.PPort_name = Seq_node[count_x]->out_pport[count_y]->edge->Edge_name + ":BP"+ID;
                temp_Opport.PPort_name = legalizeName(temp_Opport.PPort_name);
                Seq_node[count_x]->out_pport[count_y]->edge->Num_for_cut ++;
                temp_cutpair.PO_name = temp_Opport.PPort_name;
                //--------------------add new port defination to the network, but it's connection-information is empty(which is not essential)
                network->getDefManager()->addPortDef(temp_Ipport.PPort_name,DIR_IN);
                temp_Ipport.port = network->addPort(temp_Ipport.PPort_name);
                network->getDefManager()->addPortDef(temp_Opport.PPort_name,DIR_OUT);
                temp_Opport.port = network->addPort(temp_Opport.PPort_name);
                //------------------------------------------------------------------------------------------------------------
                //updata original edge& new PI connect-information
                new_boundry_pport.push_back(temp_Ipport);
                temp_cutpair.PI_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                new_boundry_pport[new_boundry_pport.size()-1].edge = Seq_node[count_x]->out_pport[count_y]->edge;// this sentence should put in front of the next one
                Seq_node[count_x]->out_pport[count_y]->edge->src_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                //updata original port & new edge & new PO connect-information
                new_boundry_pport.push_back(temp_Opport);
                Edge temp_newedge;
                //---------------------------------------8.17 update
                char edge_id[20];
                string edge_ID;
                sprintf(edge_id,"%d",new_edge_id);
                edge_ID = edge_id;
                temp_newedge.Edge_name = "new_edge_"+edge_ID;
                temp_newedge.Edge_name = legalizeName(temp_newedge.Edge_name);
                new_edge_id++;
                temp_newedge.net = network->addNet(temp_newedge.Edge_name);// add new edge to the network, but it's connection-information is empty(which is not eseential)
                //---------------------------------------------------------------------
                new_edge.push_back(temp_newedge);
                temp_cutpair.PO_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                new_edge[new_edge.size()-1].src_pport = Seq_node[count_x]->out_pport[count_y];
                Seq_node[count_x]->out_pport[count_y]->edge = &new_edge[new_edge.size()-1];
                new_edge[new_edge.size()-1].dst_pport.push_back(&new_boundry_pport[new_boundry_pport.size()-1]);
                new_boundry_pport[new_boundry_pport.size()-1].edge = &new_edge[new_edge.size()-1];
                new_edge[new_edge.size()-1].Edge_name = new_edge[new_edge.size()-1].dst_pport[0]->PPort_name +"_to_"+new_edge[new_edge.size()-1].src_pport->PPort_name;
                new_edge[new_edge.size()-1].Edge_name = legalizeName(new_edge[new_edge.size()-1].Edge_name);
                temp_cutpair.new_edge_name = new_edge[new_edge.size()-1].Edge_name;
                temp_cutpair.New_edge_connect_PI_pport = false;
                cutPairs.push_back(temp_cutpair);
            }
        }
}
//---------------------------finish preprocessing----------------------------------------------------------------------------------------


for(unsigned count_x=0;count_x<all_edge.size();count_x++)
{
    final_edge.push_back(&all_edge[count_x]);
}
for(unsigned count_x=0;count_x<new_edge.size();count_x++)
{
    final_edge.push_back(&new_edge[count_x]);
}

for(unsigned count_x=0;count_x<all_PPort[all_PPort.size()-1].size();count_x++)
{
    all_boundry_pport.push_back(&all_PPort[all_PPort.size()-1][count_x]);
};
for(unsigned count_x=0;count_x<new_boundry_pport.size();count_x++)
{
    all_boundry_pport.push_back(&new_boundry_pport[count_x]);
}

for(unsigned int count_x=0;count_x<final_edge.size();count_x++)
{
    if(final_edge[count_x]->src_pport !=NULL)
    {
        final_edge[count_x]->all_pport.push_back(final_edge[count_x]->src_pport);
    }
    for(unsigned int count_y=0;count_y<final_edge[count_x]->dst_pport.size();count_y++)
    {
        final_edge[count_x]->all_pport.push_back(final_edge[count_x]->dst_pport[count_y]);
    }
}






}
void preprocessingRuleOne(vector<Edge*>&final_edge,vector<PPort*>&all_boundry_pport,vector<PPort>&new_boundry_pport,vector<Edge>&new_edge,vector<Cutpair>&cutPairs,vector<Node*>&Seq_node,vector<vector<PPort> >&all_PPort,vector<Edge>&all_edge,Network* &network)
{
    int edgeNeedCut(0);

    for(unsigned int count_x=0;count_x<Seq_node.size();count_x++)
    {
        edgeNeedCut = edgeNeedCut + Seq_node[count_x]->in_pport.size();
        for(unsigned int count_y=0;count_y<Seq_node[count_x]->out_pport.size();count_y++)
        {
            PPort* tempPport = Seq_node[count_x]->out_pport[count_y];
            if(tempPport->edge!=NULL)
            {
                edgeNeedCut = edgeNeedCut + tempPport->edge->dst_pport.size();
            }
        }
    }



    for(unsigned int count_x=0;count_x<all_PPort[all_PPort.size()-1].size();count_x++)
    {
        Edge* tedge = all_PPort[all_PPort.size()-1][count_x].edge;

        edgeNeedCut = edgeNeedCut + tedge->dst_pport.size();
    }
    new_edge.reserve(edgeNeedCut + 20);
    new_boundry_pport.reserve(edgeNeedCut*2 + 20);
    cutPairs.reserve(edgeNeedCut + 20);



    for(unsigned int count_x=0;count_x<all_PPort[all_PPort.size()-1].size();count_x++)
    {
        PPort* tempBoundryPport = &all_PPort[all_PPort.size()-1][count_x];
        Edge* BoundryEdge = tempBoundryPport->edge;
        if(BoundryEdge->src_pport == tempBoundryPport) // if equal, then tempBoundryPport is PI
        {
            bool firstDown(false);
            for(unsigned int count_y=0;count_y<BoundryEdge->dst_pport.size();count_y++)
            {
                if(BoundryEdge->dst_pport[count_y]->isBoundryPPort==false)
                {
                    if((BoundryEdge->dst_pport[count_y]->srcNode->is_comb == true)&&(firstDown==false))
                    {
                        firstDown = true;
                    }
                    else if((BoundryEdge->dst_pport[count_y]->srcNode->is_comb == true)&&(firstDown==true))
                    {
                        PPort newPPin;
                        PPort newPPout;
                        Edge newEdge;
                        Cutpair tempCutpair;

                        tempCutpair.New_edge_connect_PI_pport = true;
                        newPPin.isBoundryPPort = true;
                        newPPout.isBoundryPPort = true;

                        char id[20];
                        sprintf(id,"%d",BoundryEdge->Num_for_cut);
                        BoundryEdge->Num_for_cut++;
                        string ID = id;
                        newPPin.PPort_name = BoundryEdge->Edge_name + "BP|"+ID;
                        newPPin.PPort_name = legalizeName(newPPin.PPort_name);

                        sprintf(id,"%d",BoundryEdge->Num_for_cut);
                        BoundryEdge->Num_for_cut++;
                        ID = id;
                        newPPout.PPort_name = BoundryEdge->Edge_name +"BP|"+ID;
                        newPPout.PPort_name = legalizeName(newPPout.PPort_name);

                        network->getDefManager()->addPortDef(newPPin.PPort_name,DIR_IN);
                        network->getDefManager()->addPortDef(newPPout.PPort_name,DIR_OUT);
                        tempCutpair.PI_name = newPPin.PPort_name;
                        tempCutpair.PO_name = newPPout.PPort_name;

                        newEdge.isNewAdd = true;
                        newEdge.Edge_name = newPPin.PPort_name +"|newEdge";
                        newEdge.Edge_name = legalizeName(newEdge.Edge_name);

                        new_edge.push_back(newEdge);
                        new_boundry_pport.push_back(newPPin);
                        new_boundry_pport[new_boundry_pport.size()-1].edge = &new_edge[new_edge.size()-1];
                        new_edge[new_edge.size()-1].src_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                        new_edge[new_edge.size()-1].dst_pport.push_back(BoundryEdge->dst_pport[count_y]);
                        BoundryEdge->dst_pport[count_y]->edge = &new_edge[new_edge.size()-1];

                        new_boundry_pport.push_back(newPPout);
                        BoundryEdge->dst_pport[count_y] = &new_boundry_pport[new_boundry_pport.size()-1];
                        new_boundry_pport[new_boundry_pport.size()-1].edge = BoundryEdge;
                        cutPairs.push_back(tempCutpair);
                    }
                }
            }
        }
    }

    for(unsigned int count_x=0;count_x<Seq_node.size();count_x++)
    {
        for(unsigned int count_y=0;count_y<Seq_node[count_x]->out_pport.size();count_y++)
        {
            Edge* outEdge = Seq_node[count_x]->out_pport[count_y]->edge;
            if(outEdge != NULL)
            {
                for(unsigned int count_z=0;count_z<outEdge->dst_pport.size();count_z++)
                {
                    PPort* tempPport = outEdge->dst_pport[count_z];
                    if((tempPport->isBoundryPPort == false)&&(tempPport->srcNode->is_comb == true))// only handle the Dstport whose srcNode is Combinational logic
                    {
                        PPort newPPin;
                        PPort newPPout;
                        Edge newEdge;
                        Cutpair tempCutpair;

                        tempCutpair.New_edge_connect_PI_pport = true;
                        newPPin.isBoundryPPort = true;
                        newPPout.isBoundryPPort = true;


                        char id[20];
                        sprintf(id,"%d",outEdge->Num_for_cut);
                        string ID = id;
                        outEdge->Num_for_cut++;
                        newPPin.PPort_name = outEdge->Edge_name + "BP|"+ID;
                        newPPin.PPort_name = legalizeName(newPPin.PPort_name);

                        sprintf(id,"%d",outEdge->Num_for_cut);
                        ID = id;
                        outEdge->Num_for_cut++;
                        newPPout.PPort_name = outEdge->Edge_name + "BP|"+ID;
                        newPPout.PPort_name = legalizeName(newPPout.PPort_name);

                        newEdge.isNewAdd = true;
                        newEdge.Edge_name = newPPin.PPort_name +"|newEdge";
                        newEdge.Edge_name = legalizeName(newEdge.Edge_name);

                        network->getDefManager()->addPortDef(newPPin.PPort_name,DIR_IN);
                        network->getDefManager()->addPortDef(newPPout.PPort_name,DIR_OUT);
                        tempCutpair.PI_name = newPPin.PPort_name;
                        tempCutpair.PO_name = newPPout.PPort_name;
                        cutPairs.push_back(tempCutpair);

                        new_edge.push_back(newEdge);
                        new_boundry_pport.push_back(newPPin);
                        new_boundry_pport[new_boundry_pport.size()-1].edge = &new_edge[new_edge.size()-1];
                        new_edge[new_edge.size()-1].src_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                        new_edge[new_edge.size()-1].dst_pport.push_back(tempPport);
                        tempPport->edge = &new_edge[new_edge.size()-1];

                        new_boundry_pport.push_back(newPPout);
                        new_boundry_pport[new_boundry_pport.size()-1].edge = outEdge;
                        outEdge->dst_pport[count_z] = &new_boundry_pport[new_boundry_pport.size()-1];
                    }
                }
            }
        }
    }

    for(unsigned int count_x=0;count_x<Seq_node.size();count_x++)
    {
        for(unsigned int count_y=0;count_y<Seq_node[count_x]->in_pport.size();count_y++)
        {
            Edge* inEdge = Seq_node[count_x]->in_pport[count_y]->edge;
            if(inEdge!=NULL)
            {
                PPort* srcPport = inEdge->src_pport;
        //		if(srcPport->isBoundryPPort == false)
                {
                    PPort newPPin;
                    PPort newPPout;
                    Edge newEdge;
                    Cutpair tempCutpair;

                    newPPin.isBoundryPPort = true;
                    newPPout.isBoundryPPort = true;
                    tempCutpair.New_edge_connect_PI_pport = true;

                    char id[20];
                    sprintf(id,"%d",inEdge->Num_for_cut);
                    inEdge->Num_for_cut++;
                    string ID = id;
                    newPPin.PPort_name = inEdge->Edge_name+"BP|"+ID;
                    newPPin.PPort_name = legalizeName(newPPin.PPort_name);

                    sprintf(id,"%d",inEdge->Num_for_cut);
                    inEdge->Num_for_cut++;
                    ID = id;
                    newPPout.PPort_name = inEdge->Edge_name +"BP|"+ID;
                    newPPout.PPort_name = legalizeName(newPPout.PPort_name);

                    newEdge.isNewAdd = true;
                    newEdge.Edge_name = newPPin.PPort_name  +"|newEdge";
                    newEdge.Edge_name = legalizeName(newEdge.Edge_name);

                    tempCutpair.PI_name = newPPin.PPort_name;
                    tempCutpair.PO_name = newPPout.PPort_name;
                    network->getDefManager()->addPortDef(newPPin.PPort_name,DIR_IN);
                    network->getDefManager()->addPortDef(newPPout.PPort_name,DIR_OUT);

                    new_boundry_pport.push_back(newPPout);
                    new_boundry_pport[new_boundry_pport.size()-1].edge = inEdge;
                    for(unsigned int count_z=0;count_z<inEdge->dst_pport.size();count_z++)
                    {
                        if(inEdge->dst_pport[count_z] == Seq_node[count_x]->in_pport[count_y])
                        {
                            inEdge->dst_pport[count_z] = &new_boundry_pport[new_boundry_pport.size()-1];
                            break;
                        }
                    }

                    new_edge.push_back(newEdge);
                    new_boundry_pport.push_back(newPPin);
                    new_boundry_pport[new_boundry_pport.size()-1].edge = &new_edge[new_edge.size()-1];
                    new_edge[new_edge.size()-1].src_pport = &new_boundry_pport[new_boundry_pport.size()-1];
                    new_edge[new_edge.size()-1].dst_pport.push_back(Seq_node[count_x]->in_pport[count_y]);
                    Seq_node[count_x]->in_pport[count_y]->edge = &new_edge[new_edge.size()-1];
					cutPairs.push_back(tempCutpair);

                }
            }
        }
    }



for(unsigned count_x=0;count_x<all_edge.size();count_x++)
{
    final_edge.push_back(&all_edge[count_x]);
}
for(unsigned count_x=0;count_x<new_edge.size();count_x++)
{
    final_edge.push_back(&new_edge[count_x]);
}

for(unsigned count_x=0;count_x<all_PPort[all_PPort.size()-1].size();count_x++)
{
    all_boundry_pport.push_back(&all_PPort[all_PPort.size()-1][count_x]);
};
for(unsigned count_x=0;count_x<new_boundry_pport.size();count_x++)
{
    all_boundry_pport.push_back(&new_boundry_pport[count_x]);
}

for(unsigned int count_x=0;count_x<final_edge.size();count_x++)
{
    if(final_edge[count_x]->src_pport !=NULL)
    {
        final_edge[count_x]->all_pport.push_back(final_edge[count_x]->src_pport);
    }
    for(unsigned int count_y=0;count_y<final_edge[count_x]->dst_pport.size();count_y++)
    {
        final_edge[count_x]->all_pport.push_back(final_edge[count_x]->dst_pport[count_y]);
    }
}






}
void clusterProcessing(vector<Node>&all_node,vector<Cluster>&final_cluster,vector<Edge*>&final_edge)
{
    for(unsigned count_x=0;count_x<all_node.size();count_x++)
{
    if(all_node[count_x].check == false)
    {
        vector<Node*>remain_node;
        Cluster temp_cluster;
        all_node[count_x].check = true;

        temp_cluster.node.push_back(&all_node[count_x]);
        if(all_node[count_x].is_comb == false)
        {
            temp_cluster.need_map = false;
            for(unsigned int count_y=0;count_y<all_node[count_x].all_pport.size();count_y++)
            {
                if(all_node[count_x].all_pport[count_y]->edge != NULL)
                {
                    if(all_node[count_x].all_pport[count_y]->edge->check == true)
                    {
                        cout<<"wrong in B"<<endl;
                    }
                    all_node[count_x].all_pport[count_y]->edge->check = true;
                    temp_cluster.edge.push_back(all_node[count_x].all_pport[count_y]->edge);
                    for(unsigned int count_z=0;count_z<all_node[count_x].all_pport[count_y]->edge->all_pport.size();count_z++)
                    {
                        if(all_node[count_x].all_pport[count_y]->edge->all_pport[count_z]->isBoundryPPort == true)
                        {

                            temp_cluster.boundry_pport.push_back(all_node[count_x].all_pport[count_y]->edge->all_pport[count_z]);
                            all_node[count_x].all_pport[count_y]->edge->all_pport[count_z]->check = true;// for examine usage, mark this boundryport's check true
                        }
                        else if(all_node[count_x].all_pport[count_y]->edge->all_pport[count_z]!=all_node[count_x].all_pport[count_y])
                        {
                            cout<<"wrong in A"<<endl;
                        }
                    }
                }
            }

        }
        else
        {
            remain_node.push_back(&all_node[count_x]);
        }

        while(remain_node.size()!=0)
        {
            Node* temp_node = remain_node[remain_node.size()-1];
            temp_node->check = true;
            remain_node.pop_back();

            for(unsigned int count_y=0;count_y<temp_node->all_pport.size();count_y++)
            {
                if(temp_node->all_pport[count_y]->edge != NULL)
                {
                    if(temp_node->all_pport[count_y]->edge->check == false)
                    {
                        Edge* temp_edge = temp_node->all_pport[count_y]->edge;
                        temp_edge->check = true;
                        temp_cluster.edge.push_back(temp_edge);

                        for(unsigned int count_z=0;count_z<temp_edge->all_pport.size();count_z++)
                        {
                            if((temp_edge->all_pport[count_z]->isBoundryPPort == true)&&(temp_edge->all_pport[count_z]->check == false))
                            {
                                temp_cluster.boundry_pport.push_back(temp_edge->all_pport[count_z]);
                                temp_edge->all_pport[count_z]->check = true;
                            }
                            else if(temp_edge->all_pport[count_z]->srcNode!=NULL)
                            {
                                if(temp_edge->all_pport[count_z]->srcNode->is_comb == false)
                                {
                                    cout<<"wrong in C"<<endl;
                                }
                                if(temp_edge->all_pport[count_z]->srcNode->check == false)
                                {
                                    temp_cluster.node.push_back(temp_edge->all_pport[count_z]->srcNode);// ver2 here should concern that a net's source and destination is a same Node
                                    temp_edge->all_pport[count_z]->srcNode->check = true;
                                    remain_node.push_back(temp_edge->all_pport[count_z]->srcNode);
                                }
                            }
                        }
                    }
                }
            }
        }

            final_cluster.push_back(temp_cluster);
    }
}

for(unsigned int count_x=0;count_x<final_edge.size();count_x++)
{
    if(final_edge[count_x]->check == false)
    {
        Cluster temp_cluster;
        temp_cluster.edge.push_back(final_edge[count_x]);

        if(final_edge[count_x]->src_pport!=NULL)
        {
            if(final_edge[count_x]->src_pport->isBoundryPPort == false)
            {
                cout<<"wrong in D"<<endl;
            }
            else
            {
            temp_cluster.boundry_pport.push_back(final_edge[count_x]->src_pport);
            }

            for(unsigned count_y=0;count_y<final_edge[count_x]->dst_pport.size();count_y++)
            {
                if(final_edge[count_x]->dst_pport[count_y]->isBoundryPPort == false)
                {
                    cout<<"wrong in E"<<endl;
                }
                else
                {
                    temp_cluster.boundry_pport.push_back(final_edge[count_x]->dst_pport[count_y]);
                }
            }
        }
        else
        {

            for(unsigned count_y=0;count_y<final_edge[count_x]->dst_pport.size();count_y++)
            {
                if(final_edge[count_x]->dst_pport[count_y]->isBoundryPPort == false)
                {
                    cout<<"wrong in EE"<<endl;
                }
                else
                {
                    temp_cluster.boundry_pport.push_back(final_edge[count_x]->dst_pport[count_y]);
                }
            }
        }

            final_cluster.push_back(temp_cluster);
    }
}



}
void createSubnetworks(vector<Subnetwork>&subcircuit,vector<Network*>&subNetworks, ExtendDefManager* extDefManager)
{

vector<Network*>output_network;
for(unsigned int count_x=0;count_x<subcircuit.size();count_x++)
{
    char id[20];
    string ID;
    sprintf(id,"%d",count_x);
//	itoa(count_x,id,10);
    ID = id;
    string sub_network_name = "sub_network_" + ID;
    Network* temp_output_network = new Network(sub_network_name,extDefManager);



    for(unsigned int count_y=0;count_y<subcircuit[count_x].member_cluster.size();count_y++)
    {

        for(unsigned int count_z=0;count_z<subcircuit[count_x].member_cluster[count_y]->edge.size();count_z++)
        {
        //	cout<<subcircuit[count_x].member_cluster[count_y]->edge[count_z]->Edge_name<<endl;
            Net* temp_net = temp_output_network->addNet(subcircuit[count_x].member_cluster[count_y]->edge[count_z]->Edge_name);
            temp_net->setUserData(subcircuit[count_x].member_cluster[count_y]->edge[count_z]);
            subcircuit[count_x].member_cluster[count_y]->edge[count_z]->new_net = temp_net;
        }

        for(unsigned int count_z=0;count_z<subcircuit[count_x].member_cluster[count_y]->boundry_pport.size();count_z++)
        {

            Port* temp_boundry_port = temp_output_network->addPort(subcircuit[count_x].member_cluster[count_y]->boundry_pport[count_z]->PPort_name);
            subcircuit[count_x].member_cluster[count_y]->boundry_pport[count_z]->new_port = temp_boundry_port;
            temp_boundry_port->setUserData(subcircuit[count_x].member_cluster[count_y]->boundry_pport[count_z]);
        }

        for(unsigned int count_z=0;count_z<subcircuit[count_x].member_cluster[count_y]->node.size();count_z++)
        {
            Block* temp_block = subcircuit[count_x].member_cluster[count_y]->node[count_z]->block->deepClone();


            temp_block->setUserData(subcircuit[count_x].member_cluster[count_y]->node[count_z]);
            temp_output_network->addBlock(temp_block);
            subcircuit[count_x].member_cluster[count_y]->node[count_z]->new_block = temp_block;
            //----------2014.8.22--------------------------------------------------------------------------------------------
            GeneralIterator<Port*> tPorts = temp_block->getAllPorts();
            while(tPorts.hasNext())
            {
                bool findout(false);
                Port* tPort = tPorts.next();
                string Port_name = tPort->getName().str();
                for(unsigned int count_p=0;count_p<subcircuit[count_x].member_cluster[count_y]->node[count_z]->all_pport.size();count_p++)
                {
                //	cout<<Port_name<<"           "<<subcircuit[count_x].member_cluster[count_y]->node[count_z]->all_pport[count_p]->PPort_name<<endl;
                    if(subcircuit[count_x].member_cluster[count_y]->node[count_z]->all_pport[count_p]->PPort_name == Port_name )
                    {
                        tPort->setUserData(subcircuit[count_x].member_cluster[count_y]->node[count_z]->all_pport[count_p]);
                        subcircuit[count_x].member_cluster[count_y]->node[count_z]->all_pport[count_p]->new_port = tPort;
                        findout = true;
                    }
                }
                if(findout == false)
                {
                    cout<<"wrong in F"<<endl;
                }
            }

            //-------------------------------------------------------------------------------------------------------
        }
    }
    output_network.push_back(temp_output_network);
}



//--------------------Connect new networks' block & boundryPort & net------------------------------------------------------


for(unsigned int count_x=0;count_x<output_network.size();count_x++)
{
    GeneralIterator<Net*> new_nets = output_network[count_x]->getAllNets();
    while(new_nets.hasNext())
    {
        Net* tNet = new_nets.next();
        Edge* tEdge =(Edge*)tNet->getUserData();
        if(tEdge->src_pport !=NULL)
        {
            tNet->addPort(tEdge->src_pport->new_port);
        }
        for(unsigned int count_y =0;count_y<tEdge->dst_pport.size();count_y++)
        {
            tNet->addPort(tEdge->dst_pport[count_y]->new_port);
        }
    }
}





//--------------need further modified---------------------------------------------
for(unsigned int count_x=0;count_x<output_network.size();count_x++)
{
    subNetworks.push_back(output_network[count_x]);
}

}



static bool netlistChecker(Network *network) {
    // valid check
    bool res = false;
    GeneralIterator<Net*> nets = network->getAllNets();
    while (nets.hasNext()) {
        Net *net = nets.next();
        // check src port
        Port * srcPort = net->getSrcPort();
        if (srcPort)
        {
            if (srcPort->getNet() != net) {
                cout << "Invalid Net: " << net->getName()
                    << "\tSource port connect to other net."
                    << endl;
                res = true;
            }
        }

        GeneralIterator<Port*> dstPorts = net->getDstPorts();
        while (dstPorts.hasNext()) {
            Port *dstPort = dstPorts.next();
            if (dstPort->getNet() != net) {
                cout << "Invalid Net: " << net->getName()
                    << "\tDstination port connect to other net."
                    << endl;
                res = true;
            }
        }
    }
    return res;
}




void cluster(Network* network,vector<Cutpair>&cutPairs,vector<Network*>&subNetworks,int partitionRuleSelection/* = 0 */)
{
//----------START ALL-----------------------------------------------------
    cout<<"this dllllllllllllllll"<<endl;
    ExtendDefManager* extDefManager = network->getDefManager();
    vector<Block*>all_block;
    vector<Port*>all_port;
    vector<Net*>all_net;
	vector<Port*>bportWithoutNet;
    int seq_logic_num(0);
    dataPreparation(all_block,all_port,all_net,network,seq_logic_num,bportWithoutNet); //block, net, port  preparation

//------------------------DATA STRUCTURE Node, PPort, Edge finished------------------------------------------------
    vector<Node>all_node(all_block.size());
    vector<Node*>Seq_node;
    vector<Node*>Comb_node;
    vector<Edge>all_edge(all_net.size());
    vector<vector<PPort> >all_PPort(all_block.size()+1);
    Seq_node.reserve(seq_logic_num+10);
    Comb_node.reserve(all_block.size()-seq_logic_num+10);

    clusterDataConstructor(all_node,Seq_node,Comb_node,all_edge,all_PPort,all_block,all_net,all_port); // Node,Edge,PPort Data constructor and Link
//----------------------Preprocessing-------------------------------------------------
	vector<Edge*>final_edge;
	vector<PPort*>all_boundry_pport;
	vector<PPort>new_boundry_pport;
	vector<Edge>new_edge;
	
	switch(partitionRuleSelection)
	{			
		case 0:	preprocessing(final_edge,all_boundry_pport,new_boundry_pport,new_edge,cutPairs,Seq_node,all_PPort,all_edge,network);break; //Original preprocessing
		case 1:	preprocessingRuleOne(final_edge,all_boundry_pport,new_boundry_pport,new_edge,cutPairs,Seq_node,all_PPort,all_edge,network); break;// Apply ruleOne preprocessing	
	}
//----------Start Cluster----------------------------------------------------
    vector<Cluster>final_cluster;
    clusterProcessing(all_node,final_cluster,final_edge);
    int total_core(4);
    vector<Subnetwork>subcircuit(total_core);
    LPT_partition(total_core,subcircuit,final_cluster);
//----------create Subnetworks-----------------------------------------------
    createSubnetworks(subcircuit,subNetworks,extDefManager);

	for(unsigned int count_x=0;count_x<bportWithoutNet.size();count_x++)
	{
		string BpName = bportWithoutNet[count_x]->getName();
		subNetworks[0]->addPort(BpName);
	}
//---------------------------------------------------------------------------
    // clear the user data point of all network
    for(unsigned int i=0;i<subNetworks.size();i++)
    {
        GeneralIterator<Net*> nets = subNetworks[i]->getAllNets();
        while(nets.hasNext())
        {
            Net* net = nets.next();
            //Edge* tEdge =(Edge*)tNet->getUserData();
            //delete tEdge;
            net->setUserData(NULL);
        }

        GeneralIterator<Block*> blocks = subNetworks[i]->getAllBlocks();
        while(blocks.hasNext())
        {
            Block* block = blocks.next();
            //Node* node =(Node*)block->getUserData();
            //delete node;
            block->setUserData(NULL);
        }

        GeneralIterator<Port*> ports = subNetworks[i]->getAllPorts();
        while(ports.hasNext())
        {
            Port* port = ports.next();
            //PPort* pport =(PPort*)port->getUserData();
            //delete pport;
            port->setUserData(NULL);
        }
    }

    netlistChecker(subNetworks[0]);
    netlistChecker(subNetworks[1]);
    netlistChecker(subNetworks[2]);
    netlistChecker(subNetworks[3]);


#ifdef __FOR_DEBUG__
	/*
	cutPairs.clear();
	for(unsigned int count_x=0;count_x<all_boundry_pport.size();count_x++)
	{
		Cutpair tempCp;
		tempCp.PI_name = all_boundry_pport[count_x]->PPort_name;
		cutPairs.push_back(tempCp);
	}

	

	int maxCluster(0);
	for(unsigned int count_x=0;count_x<final_cluster.size();count_x++)
	{
		if(final_cluster[count_x].node.size()>maxCluster)
		{
			maxCluster = final_cluster[count_x].node.size();
		}
	}
	cout<<"Max Cluster size: "<<maxCluster<<endl;


	for(unsigned int count_x=0;count_x<subNetworks.size();count_x++)
	{
		cout<<"Total #Block  "<<subNetworks[count_x]->getNumBlocks()<<endl;
	}

	int countComb(0);
	for(unsigned int count_x=0;count_x<subNetworks.size();count_x++)
	{
		GeneralIterator<Block*>aBlocks = subNetworks[count_x]->getAllBlocks();
		while(aBlocks.hasNext())
		{
			Block* aBlock = aBlocks.next();
			if(isCombinationalBlock(aBlock)==true)
			{
				countComb++;
			}
		}
		cout<<"Total #CombBlock: "<<countComb<<endl;
		countComb = 0;
	}
	*/
	bool hasBport(false);
	for(unsigned int count_x=0;count_x<cutPairs.size();count_x++)
	{
		if(cutPairs[count_x].PI_name == "CLOCK_39_netBP|1")
		{
			hasBport = true;
			break;
		}
		else if(cutPairs[count_x].PO_name == "CLOCK_39_netBP|1")
		{
			hasBport = true;
			break;
		}
	}

	for(unsigned int count_x=0;count_x<subNetworks.size();count_x++)
	{
		cout<<count_x<<endl;
		if(subNetworks[count_x]->getPort("\CLOCK_39_netBP|1") !=NULL)
		{
			hasBport = true;
			break;
		}
	}


for(unsigned int count_x=0;count_x<subNetworks.size();count_x++)
{
	GeneralIterator<Net*>ANets = subNetworks[count_x]->getAllNets();
	while(ANets.hasNext())
	{
		Net* ANet = ANets.next();
		Port* srcPort = ANet->getSrcPort();
		if(srcPort->getBlock() == NULL)
		{
			GeneralIterator<Port*>netDstports = ANet->getDstPorts();
			while(netDstports.hasNext())
			{
				Port* netDstport = netDstports.next();
				if(netDstport->getBlock() == NULL)
				{
					cout<<"Net: "<<ANet->getName()<<endl;
					break;
				}
			}
		}
	}
}
		








#endif


    cout << "cluster done." << endl;
};

