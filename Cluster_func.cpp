#include "Cluster_func.h"

bool wirte_subnetwork(vector<Network*>sub_network,vector<string>&subnetwork_fileName)
{

    for(unsigned int count_x=0;count_x<sub_network.size();count_x++)
    {
        ofstream output_file;
        char id[20];
        string ID;
        //itoa(count_x,id,10);
        sprintf(id, "%d", count_x);
        ID = id;

        string file_name = "subnetwork_part"+ID;

        subnetwork_fileName.push_back(file_name);

        output_file.open(file_name);
        if(output_file.fail())
        {
            cout<<"fail to open "<<file_name<<endl;
            return 0;
        }
        bool write_out;
        write_out = NetworkWriter::writeVerilog(sub_network[count_x],output_file);
        if(write_out == false)
        {
            cout<<"fail to write "<<file_name<<endl;
            return 0;
        }
        else
        {
            output_file.close();
        }

    }
    return true;
}


vector<Cluster*> Sorting(vector<Cluster*>&p_cluster)
{
    if(p_cluster.size()==1)
    {
        vector<Cluster*> sort_cluster;
        sort_cluster.push_back(p_cluster[0]);
        return sort_cluster;
    }
    else if(p_cluster.size()==2)
    {
        vector<Cluster*> sort_cluster;
        if(p_cluster[0]->node.size()>=p_cluster[1]->node.size())
        {
            sort_cluster.push_back(p_cluster[1]);
            sort_cluster.push_back(p_cluster[0]);
        }
        else
        {
            sort_cluster.push_back(p_cluster[0]);
            sort_cluster.push_back(p_cluster[1]);
        }
        return sort_cluster;
    }
    else
    {
        unsigned int half_index = p_cluster.size()/2;
        vector<Cluster*>first_part_cluster;
        vector<Cluster*>second_part_cluster;
        for(unsigned int count_cc=0;count_cc < half_index;count_cc++)
        {
            first_part_cluster.push_back(p_cluster[count_cc]);
            second_part_cluster.push_back(p_cluster[half_index+count_cc]);
        }
        if((half_index*2)!=p_cluster.size())
        {
            second_part_cluster.push_back(p_cluster[p_cluster.size()-1]);
        }
        vector<Cluster*>sorted_first_part;
        vector<Cluster*>sorted_second_part;

        sorted_first_part = Sorting(first_part_cluster);
        sorted_second_part = Sorting(second_part_cluster);
        vector<Cluster*>sort_cluster;
        int index_first = sorted_first_part.size();
        int index_second = sorted_second_part.size();

        while((index_first!= 0)||(index_second!=0))
        {
            if((index_first!= 0)&&(index_second == 0))
            {
                sort_cluster.push_back(sorted_first_part[sorted_first_part.size() - index_first]);
                index_first--;
            }
            else if((index_first == 0)&&(index_second!=0))
            {
                sort_cluster.push_back(sorted_second_part[sorted_second_part.size() - index_second]);
                index_second--;
            }
            else
            {
                if((sorted_first_part[sorted_first_part.size()-index_first]->node.size())<=(sorted_second_part[sorted_second_part.size()-index_second]->node.size()))
                {
                    sort_cluster.push_back(sorted_first_part[sorted_first_part.size()-index_first]);
                    index_first--;
                }
                else
                {
                    sort_cluster.push_back(sorted_second_part[sorted_second_part.size()-index_second]);
                    index_second--;
                }
            }
        }
        return sort_cluster;
    }
}
void LPT_partition(unsigned int num_processor,vector<Subnetwork>&partition_in,vector<Cluster>&Cluster_in)
{
    vector<Cluster*>P_cluster;
    for(unsigned int count_c=0;count_c < Cluster_in.size();count_c++)
    {
        P_cluster.push_back(&Cluster_in[count_c]);
    }

    vector<Cluster*>sorted_p_cluster;

    sorted_p_cluster = Sorting(P_cluster);// sorted_p_cluster is increasing order

    for(unsigned int count_x=0;count_x < sorted_p_cluster.size();count_x++)
    {
        int min_load_index(0);
        int min_load_size(999999);
        for(unsigned int count_y=0;count_y < num_processor;count_y++)//find min load processor
        {
            if(partition_in[count_y].total_node<min_load_size)
            {
                min_load_size = partition_in[count_y].total_node;
                min_load_index = count_y;
            }
        }

        partition_in[min_load_index].member_cluster.push_back(sorted_p_cluster[sorted_p_cluster.size()-1-count_x]);
        partition_in[min_load_index].total_node = partition_in[min_load_index].total_node + sorted_p_cluster[sorted_p_cluster.size()-1-count_x]->node.size();
    }
}

